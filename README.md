# Test-Driven Development with Python, Flask, and Docker

[![pipeline status](https://gitlab.com/niltongmjunior/flask-tdd-docker/badges/main/pipeline.svg)](https://gitlab.com/niltongmjunior/flask-tdd-docker/commits/main)

This repository contains the contents for the [__Test-Driven Development with Python, Flask, and Docker__](https://testdriven.io/courses/tdd-flask/) course from testdriven.io

The course covers:

- Python
- Flask (with RESTX, SQLAlchemy and Admin)
- Docker
- Postgres
- Gunicorn
- Swagger/OpenAPI
- TDD (with pytest and Coverage.py)
- Linting (with flake8, black and isort)
- HTTPie
- CI/CD pipelines on Gitlab and deployment on Heroku

This app is currently running on [Heroku (click me to ping!)](https://flask-tdd-docker-nilton.herokuapp.com/ping)

API documentation can be seen [here](https://flask-tdd-docker-nilton.herokuapp.com/doc)
